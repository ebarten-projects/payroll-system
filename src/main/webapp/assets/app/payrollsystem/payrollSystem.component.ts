import { Component, Input } from '@angular/core';
import { EmployeeInfo} from './dto/employeeInfo';
import { PayslipInfo } from './dto/payslipInfo';
import { Payslip } from './payslip';
import { PayslipService } from './payslip.service';

@Component({
    selector: 'payroll-system',
    providers: [ PayslipService ],
    templateUrl: './payrollSystem.component.html',
    styleUrls: ['./payrollSystem.component.css']
})
export class PayrollSystemComponent {

    employeeInfo = new EmployeeInfo();
    payslip = new Payslip(false);
    hasError = false;
    errorMessage = "";

    constructor(private payrollService : PayslipService){
    }


    /**
     * The function generates the payslip and submits it to the server.
     */
    generatePayslip(){
        var observable = this.payrollService.generatePayslip(this.payslip, this.employeeInfo);
        observable.subscribe(
            data => this.onPaymentSuccess(),
            error => this.onPaymentError(error)
        );
    }

    /**
     * The function hides the Payment section and displays the passed error message instead.
     * @param errorMessage The error message to display.
     */
    private onPaymentError(errorMessage){
        this.errorMessage = errorMessage;
        this.hasError = true;
        this.payslip.show = false;
    }

    /**
     * The function makes sure that the Payment section is displayed.
     */
    private onPaymentSuccess(){
        this.hasError = false;
        this.payslip.show = true;
    }

}
