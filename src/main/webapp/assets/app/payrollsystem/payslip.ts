import {PayslipInfo } from './dto/payslipInfo';

export class Payslip {

    constructor (
        public show: boolean,
        public info?:PayslipInfo
    ){
        if (info === undefined) {
            this.info = new PayslipInfo();
        }
    }

}
