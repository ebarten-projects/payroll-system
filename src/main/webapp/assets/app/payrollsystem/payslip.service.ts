import { Injectable } from '@angular/core';
import { PayslipInfo } from './dto/payslipInfo';
import { EmployeeInfo } from './dto/employeeInfo';
import { Payslip } from './payslip';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

export const TAX_BRACKET_1 : number = 18200;
export const TAX_BRACKET_2 : number = 37000;
export const TAX_BRACKET_3 : number = 80000;
export const TAX_BRACKET_4 : number = 180000;

@Injectable()
export class PayslipService {

    private payUrl = "http://localhost/api/payroll/pay";

    constructor(private http: Http){
    }

    generatePayslip(payslip:Payslip, employeeInfo : EmployeeInfo) : Observable<any> {
        var superRate = employeeInfo.superRate;
        var salary = employeeInfo.salary;

        // Update payslip data prior to payment
        payslip.info.firstName = employeeInfo.firstName;
        payslip.info.lastName = employeeInfo.lastName;
        payslip.info.annualIncome = salary;
        payslip.info.grossIncome = Math.round(salary / 12);
        payslip.info.incomeTax = this.calculateIncomeTax(salary);
        payslip.info.netIncome = payslip.info.grossIncome - payslip.info.incomeTax;
        payslip.info.payFrequency = 'Monthly';
        payslip.info.superRate = superRate;
        payslip.info.superAnnuation = Math.round(payslip.info.grossIncome * superRate / 100);
        payslip.info.payDate = new Date();
        payslip.info.pay = payslip.info.netIncome - payslip.info.superAnnuation;


        // Send the payment request
        var observable = this.sendPaymentRequest(payslip.info);

        return observable;
    }

    calculateIncomeTax(salary:number) {
        var incomeTax = 0;

        if (TAX_BRACKET_1 < salary && salary <= TAX_BRACKET_2) {
            incomeTax = 0.19*(salary - TAX_BRACKET_1);
        } else if (TAX_BRACKET_2 < salary && salary <= TAX_BRACKET_3) {
            incomeTax = 3572 + 0.325*(salary - TAX_BRACKET_2);
        } else if (TAX_BRACKET_3 < salary && salary <= TAX_BRACKET_4) {
            incomeTax = 17547 + 0.37*(salary - TAX_BRACKET_3);
        } else {
            incomeTax = 17547 + 0.45*(salary - TAX_BRACKET_4);
        }

        incomeTax = Math.round(incomeTax / 12);

        return incomeTax;
    }

    /**
     * The function sends a HTTP request for the payment operation.
     * @param payslipInfo The payslip to submit.
     * @returns {Observable<T>} The observable to act on and process any data or error.
     */
    private sendPaymentRequest(payslipInfo : PayslipInfo) : Observable<any> {

        // Construct HTTP headers
        var payload = JSON.stringify(payslipInfo);
        var headers = new Headers({
            'Content-Type': 'application/json'
        });
        var options = new RequestOptions({ headers: headers});

        // Send the POST request and handle data
        var observable = this.http.post(this.payUrl, payload, options)
            .catch(this.handlePaymentError);
        return observable;
    }

    /**
     * The function handles any errors resulting from the HTTP payment request.
     * @param error The error to process/
     * @returns An ErrorObservable
     */
    private handlePaymentError(error) {
        var errorMessage: string = error.message ? error.message : error.toString();
        if (error.status === 409) {
            errorMessage = 'Payment has already been made';
        } else {
            errorMessage = "Unknown Server error ! Please try again.";
            console.log(errorMessage);
        }

        return Observable.throw(errorMessage);
    }
}
