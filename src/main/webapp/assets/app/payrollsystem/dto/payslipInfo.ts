export class PayslipInfo {

    constructor (
        public firstName?:string,
        public lastName?:string,
        public payDate?:Date,
        public payFrequency?:string,
        public annualIncome?:number,
        public grossIncome?:number,
        public incomeTax?:number,
        public netIncome?:number,
        public superRate?:number,
        public superAnnuation?:number,
        public pay?:number
    ){}

}
