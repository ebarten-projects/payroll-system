export class EmployeeInfo {

    constructor (
        public firstName?:string,
        public lastName?:string,
        public salary?:number,
        public superRate?:number
    ){}

}