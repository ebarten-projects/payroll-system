package au.com.ebarten.appcore.webserver;

/**
 * The class is used by the web configuration. It defines the cache control configuration item structure.
 */
public class CacheControl {

    /**The type of cache*/
    private String cacheType;

    /**The cache max age.*/
    private int maxAge;

    /**
     * The constructor of this class.
     */
    public CacheControl() {
    }

    /**
     * The constructor of this class.
     * @param cacheType The cache type.
     * @param maxAge The cache max age.
     */
    public CacheControl(String cacheType, int maxAge) {
        this.cacheType = cacheType.toLowerCase();
        this.maxAge = maxAge;
    }

    /**
     * The method returns the cache type.
     * @return The cache type.
     */
    public String getCacheType() {
        return cacheType;
    }

    /**
     * The method sets the cache type.
     * @param cacheType The cache type.
     */
    public void setCacheType(String cacheType) {
        this.cacheType = cacheType.toLowerCase();
    }

    /**
     * The method returns the cache's max age.
     * @return The cache's max age.
     */
    public int getMaxAge() {
        return maxAge;
    }

    /**
     * The method sets the cache's max age.
     * @param maxAge The cache's max age.
     */
    public void setMaxAge(int maxAge) {
        this.maxAge = maxAge;
    }

}
