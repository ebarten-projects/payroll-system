package au.com.ebarten.appcore.dto;

import java.util.Date;

/**
 * A DTO to hold the user details required for payroll.
 */
public class PayslipDto {
    /**The first name of the user this payment is for.*/
    private String firstName;
    /**The last name of the user this payment is for.*/
    private String lastName;
    /**The user's annual income in this payment.*/
    private double annualIncome;
    /**The rate used to calculate the super annuation.*/
    private double superRate;
    /**The super annuation amount captured in this user payment.*/
    private double superAnnuation;
    /**The payment date.*/
    private Date payDate;
    /**The pay frequency.*/
    private String payFrequency;
    /**The user's gross income.*/
    private double grossIncome;
    /**The user's net income.*/
    private double netIncome;
    /**The tax applied on the income.*/
    private double incomeTax;
    /**The amount paid to the user in this payment.*/
    private double pay;

    /**
     * The method returns the first name of the user this payment is for.
     * @return The first name of the user this payment is for.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * The method sets the first name of the user this payment is for.
     * @param firstName The first name of the user this payment is for.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * The method returns the last name of the user this payment is for.
     * @return The last name of the user this payment is for.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * The method sets the last name of the user this payment is for.
     * @param lastName The last name of the user this payment is for.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * The method returns the user's annual income in this payment.
     * @return The user's annual income in this payment.
     */
    public double getAnnualIncome() {
        return annualIncome;
    }

    /**
     * The method sets the user's annual income in this payment.
     * @param annualIncome The user's annual income in this payment.
     */
    public void setAnnualIncome(double annualIncome) {
        this.annualIncome = annualIncome;
    }

    /**
     * The method returns the rate used to calculate the super annuation.
     * @return The rate used to calculate the super annuation.
     */
    public double getSuperRate() {
        return superRate;
    }

    /**
     * The method sets the rate used to calculate the super annuation.
     * @param superRate The rate used to calculate the super annuation.
     */
    public void setSuperRate(double superRate) {
        this.superRate = superRate;
    }

    /**
     * The method returns the super annuation amount captured in this user payment.
     * @return The super annuation amount captured in this user payment.
     */
    public double getSuperAnnuation() {
        return superAnnuation;
    }

    /**
     * The method sets the super annuation amount captured in this user payment.
     * @param superAnnuation The super annuation amount captured in this user payment.
     */
    public void setSuperAnnuation(double superAnnuation) {
        this.superAnnuation = superAnnuation;
    }

    /**
     * The method returns the payment date.
     * @return The payment date.
     */
    public Date getPayDate() {
        return payDate;
    }

    /**
     * The method sets the payment date.
     * @param payDate The payment date.
     */
    public void setPayDate(Date payDate) {
        this.payDate = payDate;
    }

    /**
     * The method returns the pay frequency.
     * @return The pay frequency.
     */
    public String getPayFrequency() {
        return payFrequency;
    }

    /**
     * The method sets the pay frequency.
     * @param payFrequency The pay frequency.
     */
    public void setPayFrequency(String payFrequency) {
        this.payFrequency = payFrequency;
    }

    /**
     * The method returns the user's gross income.
     * @return The user's gross income.
     */
    public double getGrossIncome() {
        return grossIncome;
    }

    /**
     * The method sets the user's gross income.
     * @param grossIncome The user's gross income.
     */
    public void setGrossIncome(double grossIncome) {
        this.grossIncome = grossIncome;
    }

    /**
     * The method returns the user's net income.
     * @return The user's net income.
     */
    public double getNetIncome() {
        return netIncome;
    }

    /**
     * The method sets the user's net income.
     * @param netIncome The user's net income.
     */
    public void setNetIncome(double netIncome) {
        this.netIncome = netIncome;
    }

    /**
     * The method returns the tax applied on the income.
     * @return The tax applied on the income.
     */
    public double getIncomeTax() {
        return incomeTax;
    }

    /**
     * The method sets the tax applied on the income.
     * @param incomeTax The tax applied on the income.
     */
    public void setIncomeTax(double incomeTax) {
        this.incomeTax = incomeTax;
    }

    /**
     * The method returns the amount paid to the user in this payment.
     * @return The amount paid to the user in this payment.
     */
    public double getPay() {
        return pay;
    }

    /**
     * The method sets the amount paid to the user in this payment.
     * @param pay The amount paid to the user in this payment.
     */
    public void setPay(double pay) {
        this.pay = pay;
    }
}
