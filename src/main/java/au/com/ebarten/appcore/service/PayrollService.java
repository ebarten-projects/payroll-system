package au.com.ebarten.appcore.service;

import au.com.ebarten.appcore.dao.UserPaymentDao;
import au.com.ebarten.appcore.dto.PayslipDto;
import au.com.ebarten.appcore.entity.UserPaymentEntity;
import io.dropwizard.hibernate.UnitOfWork;
import org.hibernate.SessionFactory;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * This is the main payroll service class that will expose the application endpoints directly related to the
 * payroll system
 */
@Path("/payroll")
public class PayrollService {

    private UserPaymentDao _userDao;

    public PayrollService(UserPaymentDao userDao){
        _userDao = userDao;
    }

    /**
     * The method checks that the payment for the current user for this month hasn't been made and persists it. If the payment has been made, it returns a HTTP CONFLICT (409) response.
     * @param httpHeaders The incoming request's HTTP headers.
     * @param payslipDto The payslip DTO to persist.
     * @return HTTP OK(200) or CONFLICT(409)
     */
    @POST
    @Path("/pay")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @UnitOfWork
    public Response pay(@Context HttpHeaders httpHeaders, PayslipDto payslipDto) {
        Response response = null;

        // Get user first & last name
        String firstName = payslipDto.getFirstName();
        String lastName = payslipDto.getLastName();
        Date payDate = payslipDto.getPayDate();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(payDate);
        int month = calendar.get(Calendar.MONTH) + 1;

        // Get all user payments
        UserPaymentEntity userPaymentEntity = _userDao.find(firstName, lastName, month);
        if(userPaymentEntity == null) {
            // If the user does not exist create it.
            userPaymentEntity = new UserPaymentEntity(firstName, lastName, month);
            updatePaymentData(userPaymentEntity, payslipDto);
            // Save it to the DB
            _userDao.saveOrUpdate(userPaymentEntity);

            response = Response.ok().build();
        } else{
            response = Response.status(Response.Status.CONFLICT).build();
        }
        return response;
    }

    /**
     * The method updates the payment data received by the payslipSto onto the passed userPaymentEntity.
     * @param userPaymentEntity The user entity to populate with the passed payslipDto
     * @param payslipDto The payslip data to update the passed userPaymentEntity with.
     */
    private void updatePaymentData(UserPaymentEntity userPaymentEntity, PayslipDto payslipDto) {
        double annualIncome = payslipDto.getAnnualIncome();
        double grossIncome = payslipDto.getGrossIncome();
        double netIncome = payslipDto.getNetIncome();
        double incomeTax = payslipDto.getIncomeTax();
        double pay = payslipDto.getPay();
        double superAnnuation = payslipDto.getSuperAnnuation();
        double superRate = payslipDto.getSuperRate();
        Date payDate = payslipDto.getPayDate();
        String payFrequency = payslipDto.getPayFrequency();

        userPaymentEntity.setAnnualIncome(annualIncome);
        userPaymentEntity.setGrossIncome(grossIncome);
        userPaymentEntity.setNetIncome(netIncome);
        userPaymentEntity.setIncomeTax(incomeTax);
        userPaymentEntity.setPay(pay);
        userPaymentEntity.setSuperAnnuation(superAnnuation);
        userPaymentEntity.setSuperRate(superRate);
        userPaymentEntity.setPayDate(payDate);
        userPaymentEntity.setPayFrequency(payFrequency);
    }

}
