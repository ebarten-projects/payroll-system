package au.com.ebarten.appcore.dao;

import au.com.ebarten.appcore.entity.PaymentKey;
import au.com.ebarten.appcore.entity.UserPaymentEntity;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

/**
 * The payroll Dao to handle DB access to all required information for making a payment for a user.
 */
public class UserPaymentDao extends AbstractDAO<UserPaymentEntity> {

    /**
     * Creates a new DAO with a given session provider.
     *
     * @param sessionFactory a session provider
     */
    public UserPaymentDao(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    /**
     * The method looks up the user payment for the passed month and user details and returns it as a payment entity.
     * @param firstName The user's first name.
     * @param lastName The user's last name.
     * @param month The payment month for the payment represented by this entity.
     * @return A payment entity for a persisted user payment.
     */
    public UserPaymentEntity find(String firstName, String lastName, int month){
        UserPaymentEntity userPaymentEntity = (UserPaymentEntity) currentSession().get(UserPaymentEntity.class, new PaymentKey(firstName, lastName, month));

        return userPaymentEntity;
    }

    /**
     * The method persists the passed user entity to the DB.
     * @param userPaymentEntity The payment entity to persist.
     */
    public void saveOrUpdate(UserPaymentEntity userPaymentEntity) {
        currentSession().saveOrUpdate(userPaymentEntity);
    }


}
