package au.com.ebarten.appcore.entity;

import javax.persistence.Column;
import java.io.Serializable;

/**
 * A pojo for interacting with the user table.
 */
public class PaymentKey implements Serializable {

    /**The first name of the user the payment is for.*/
    @Column(name = "\"firstName\"")
    private String _firstName;

    /**The last name of the user the payment is for.*/
    @Column(name = "\"lastName\"")
    private String _lastName;

    /**The month of the payment.*/
    @Column(name = "\"month\"")
    private int _month;

    /**
     * The constructor of this class.
     */
    public PaymentKey() {
    }

    /**
     * The constructor of this class.
     * @param firstName The first name of the user the payment is for.
     * @param lastName The last name of the user the payment is for.
     * @param month The month of the payment.
     */
    public PaymentKey(String firstName, String lastName, int month) {
        _firstName = firstName;
        _lastName = lastName;
        _month = month;
    }

    /**
     * The method returns the first name of the user the payment is for.
     * @return The first name of the user the payment is for.
     */
    public String getFirstName() {
        return _firstName;
    }

    /**
     * The method sets the first name of the user the payment is for.
     * @param firstName The first name of the user the payment is for.
     */
    public void setFirstName(String firstName) {
        _firstName = firstName;
    }

    /**
     * The method returns the last name of the user the payment is for.
     * @return The last name of the user the payment is for.
     */
    public String getLastName() {
        return _lastName;
    }

    public void setLastName(String lastName) {
        _lastName = lastName;
    }

    /**
     * The method returns an integer representing the month of the payment.
     * @return An integer representing the month of the payment.
     */
    public int getMonth() {
        return _month;
    }

    /**
     * The method sets an integer representing the month of the payment.
     * @param month An integer representing the month of the payment.
     */
    public void setMonth(int month) {
        _month = month;
    }
}
