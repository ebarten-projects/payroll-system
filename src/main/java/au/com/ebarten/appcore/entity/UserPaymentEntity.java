package au.com.ebarten.appcore.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * The User payment entity which defines the payments table.
 */
@Entity
@Table(name="payments")
public class UserPaymentEntity implements Serializable {

    @EmbeddedId
    private PaymentKey _paymentKey;

    /**The user's annual income in this payment.*/
    @Column(name = "annualIncome")
    private double _annualIncome;

    /**The rate used to calculate the super annuation.*/
    @Column(name = "superRate")
    private double _superRate;

    /**The super annuation amount captured in this user payment.*/
    @Column(name = "superAnnuation")
    private double _superAnnuation;

    /**The payment date.*/
    @Column(name = "payDate")
    private Date _payDate;

    /**The pay frequency.*/
    @Column(name = "payFrequency")
    private String _payFrequency;

    /**The user's gross income.*/
    @Column(name = "grossIncome")
    private double _grossIncome;

    /**The user's net income.*/
    @Column(name = "netIncome")
    private double _netIncome;

    /**The tax applied on the income.*/
    @Column(name = "incomeTax")
    private double _incomeTax;

    /**The amount paid to the user in this payment.*/
    @Column(name = "pay")
    private double _pay;

    /**
     * The constructor of this class.
     */
    public UserPaymentEntity() {
    }

    /**
     * The constructor of this class.
     * @param firstName The first name of the user the payment is for.
     * @param lastName The last name of the user the payment is for.
     * @param month An integer representing the month of the payment.
     */
    public UserPaymentEntity(String firstName, String lastName, int month) {
        _paymentKey = new PaymentKey(firstName, lastName, month);
    }

    /**
     * The method returns the user's annual income in this payment.
     * @return The user's annual income in this payment.
     */
    public double getAnnualIncome() {
        return _annualIncome;
    }

    /**
     * The method sets the user's annual income in this payment.
     * @param annualIncome The user's annual income in this payment.
     */
    public void setAnnualIncome(double annualIncome) {
        _annualIncome = annualIncome;
    }

    /**
     * The method returns the rate used to calculate the super annuation.
     * @return The rate used to calculate the super annuation.
     */
    public double getSuperRate() {
        return _superRate;
    }

    /**
     * The method sets the rate used to calculate the super annuation.
     * @param superRate The rate used to calculate the super annuation.
     */
    public void setSuperRate(double superRate) {
        _superRate = superRate;
    }

    /**
     * The method returns the super annuation amount captured in this user payment.
     * @return The super annuation amount captured in this user payment.
     */
    public double getSuperAnnuation() {
        return _superAnnuation;
    }

    /**
     * The method sets the super annuation amount captured in this user payment.
     * @param superAnnuation The super annuation amount captured in this user payment.
     */
    public void setSuperAnnuation(double superAnnuation) {
        _superAnnuation = superAnnuation;
    }

    /**
     * The method returns the payment date.
     * @return The payment date.
     */
    public Date getPayDate() {
        return _payDate;
    }

    /**
     * The method sets the payment date.
     * @param payDate The payment date.
     */
    public void setPayDate(Date payDate) {
        _payDate = payDate;
    }

    /**
     * The method returns the pay frequency.
     * @return The pay frequency.
     */
    public String getPayFrequency() {
        return _payFrequency;
    }

    /**
     * The method sets the pay frequency.
     * @param payFrequency The pay frequency.
     */
    public void setPayFrequency(String payFrequency) {
        _payFrequency = payFrequency;
    }

    /**
     * The method returns the user's gross income.
     * @return The user's gross income.
     */
    public double getGrossIncome() {
        return _grossIncome;
    }

    /**
     * The method sets the user's gross income.
     * @param grossIncome The user's gross income.
     */
    public void setGrossIncome(double grossIncome) {
        _grossIncome = grossIncome;
    }

    /**
     * The method returns the user's net income.
     * @return The user's net income.
     */
    public double getNetIncome() {
        return _netIncome;
    }

    /**
     * The method sets the user's net income.
     * @param netIncome The user's net income.
     */
    public void setNetIncome(double netIncome) {
        _netIncome = netIncome;
    }

    /**
     * The method returns the tax applied on the income.
     * @return The tax applied on the income.
     */
    public double getIncomeTax() {
        return _incomeTax;
    }

    /**
     * The method sets the tax applied on the income.
     * @param incomeTax The tax applied on the income.
     */
    public void setIncomeTax(double incomeTax) {
        _incomeTax = incomeTax;
    }

    /**
     * The method returns the amount paid to the user in this payment.
     * @return The amount paid to the user in this payment.
     */
    public double getPay() {
        return _pay;
    }

    /**
     * The method sets the amount paid to the user in this payment.
     * @param pay The amount paid to the user in this payment.
     */
    public void setPay(double pay) {
        _pay = pay;
    }

}
