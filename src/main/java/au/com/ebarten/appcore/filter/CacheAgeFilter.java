package au.com.ebarten.appcore.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.HttpHeaders;
import java.io.IOException;

/**
 * A filter which will send the "max-age=0" header in every response that goes out of this server. This allows faster
 * refresh for server files on the browser.
 */
public class CacheAgeFilter implements Filter {


    private static final String cacheSettings = "max-age=";

    private int maxAge =0;

    public CacheAgeFilter(int maxAge) {
        this.maxAge = maxAge;
    }

    public CacheAgeFilter() {
    }

    @Override
    public void doFilter(ServletRequest request,  ServletResponse response,
             FilterChain chain) throws IOException, ServletException {
        if (response instanceof HttpServletResponse) {
            final HttpServletResponse resp = (HttpServletResponse) response;
            resp.setHeader(HttpHeaders.CACHE_CONTROL, cacheSettings + maxAge);
        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() { /* unused */ }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException { /* unused */ }
}