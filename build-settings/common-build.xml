<project name="common-build"
         xmlns:ivy="ivy.tasks">

    <!--Load common properties-->
    <property file="build-settings/common-build.properties"/>

    <taskdef resource="net/sf/antcontrib/antlib.xml"/>
    <taskdef resource="org/apache/ivy/ant/antlib.xml" uri="ivy.tasks"/>

    <osfamily property="os.family"/>

    <!-- The macro uses angular-cli to build the web assets in the specified build mode into the specified output path. -->
    <macrodef name="ng-cli-build">
        <attribute name="appDir" description="The application base dir"/>
        <attribute name="outputPath" description="The output path to build into"/>
        <attribute name="buildType" default="-prod" description="(O) The default build type. By default: '-prod'"/>
        <sequential>
            <echo message="appDir = @{appDir}"/>
            <echo message="outputPath = @{outputPath}"/>
            <echo message="command = ng build '@{buildType}' -op '@{outputPath}'"/>
            <shellscript shell="sh" dir="@{appDir}">
                ng build '@{buildType}' -op '@{outputPath}'

                exit 0
            </shellscript>
        </sequential>
    </macrodef>

    <!-- =================================
         target: -init

         The target initializes the build script depending on the OS that it is being executed in.
         ================================= -->
    <target name="-init">

        <!--Set paths required for the build-->
        <path id="lib.path.id">
            <fileset dir="${lib.dir}"/>
        </path>

        <mkdir dir="${lib.dir}"/>

	    <!-- Load Ivy -->
        <ivy:settings file="${ivy.settings.file}"/>
    </target>

    <!-- =================================
         target: clean-cache

         The target cleans the local machine's cache.
         ================================= -->
    <target name="clean-cache" depends="-init" description="cleans the local machine's cache.">
        <if>
            <available file="${user.cache.repo.location}"/>
            <then>
                <input message="This will delete directory: ${user.cache.repo.location}! Press Enter to continue..."/>
                <delete dir="${user.cache.repo.location}"/>
            </then>
        </if>
    </target>

    <!-- =================================
         target: clean-target
         ================================= -->
    <target name="clean-target" depends="-init" description="cleans the project built files">
        <delete includeemptydirs="true" dir="${target.dir}"/>
    </target>

    <!-- =================================
     target: -determine-os

     The target determines if the OS is windows or Linux
     ================================= -->
    <target name="-determine-os">
        <propertyregex property="os.windows" input="${os.family}" regexp="^.*(windows).*$" replace="$1" global="true"/>
    </target>

    <!-- =================================
         target: -resolve

         The target resolve and retrieve dependencies with ivy
         ================================= -->
    <target name="-resolve" depends="-init">

        <!-- the call to resolve is not mandatory, retrieve makes an implicit call if we don't -->
        <ivy:resolve file="${ivy.file}"/>
        <ivy:retrieve pattern="${lib-base.dir}/[conf]/[artifact]-[revision].[ext]" type="${ivy.retrieve.types}"/>
    </target>

    <!-- =================================
         target: report
         ================================= -->
    <target name="report" depends="-resolve" description="generates a report of dependencies">
        <ivy:report todir="${report.dir}"/>
    </target>

    <target name="-compile-src">
        <mkdir dir="${classes.dir}"/>

        <!--Construct all source files that requires compilation-->
        <path id="compile.path">
            <pathelement location="${src.java.dir}"/>
        </path>

        <!--Compile the sources-->
        <if>
            <resourceexists>
                <file file="${src.java.dir}"/>
            </resourceexists>
            <then>
                <javac destdir="${classes.dir}" classpathref="lib.path.id" debug="true"
                       includeAntRuntime="false" target="${target.jvm}">
                    <src refid="compile.path"/>
                </javac>
                <property name="compile.done" value="true"/>
            </then>
            <else>
                <echo message="No changes made for: project ${ant.project.name} sources. Compile operation skipped."
                      level="debug"/>
            </else>
        </if>
    </target>

    <target name="-compile-web">

        <!--Compile the sources-->
        <if>
            <resourceexists>
                <file file="${src.web.dir}"/>
            </resourceexists>
            <then>
                <mkdir dir="${assets.dir}"/>

                <ng-cli-build appDir="${src.web.dir}" outputPath="${assets.dir}"/>
            </then>
            <else>
                <echo message="Web assets not found for project: ${ant.project.name}. Compile operation skipped."
                      level="debug"/>
            </else>
        </if>
    </target>

    <!-- =================================
         target: compile
         ================================= -->
    <target name="compile" depends="-resolve, -compile-src, -compile-web" description="compiles the project">
        <!--Copy any resources into the classes dir -->
        <if>
            <available file="${src.resources.dir}"/>
            <then>
                <copy todir="${classes.dir}">
                    <fileset dir="${src.resources.dir}">
                        <include name="**/*.*"/>
                    </fileset>
                </copy>
            </then>
        </if>


    </target>

    <!-- =================================
         target: jar

         Create a jar file of the compiled main source.
         ================================= -->
    <target name="jar" depends="clean-target, -init, compile"
            description="make a jar file for this project">

        <property name="jar.file" value="${target.dir}/${ivy.module}.jar"/>

        <!--If no main class is defined: make sure that the property is defined.-->
        <property name="main.class" value=" "/>

        <path id="classpath">
            <fileset dir="${lib.dir}"/>
        </path>

        <manifestclasspath property="jar.classpath"
                           jarfile="${jar.file}">
            <classpath refid="classpath"/>
        </manifestclasspath>

        <if>
            <isset property="dev.mode"/>
            <then>
                <mkdir dir="${target.dir}/dev"/>
                <property name="manifest.classpath" value="${jar.classpath} dev/ "/>

                <!--Build the jar file-->
                <jar destfile="${jar.file}">
                    <fileset dir="${classes.dir}"/>
                    <manifest>
                        <attribute name="Built-By" value="${user.name}"/>
                        <attribute name="Class-Path" value="${manifest.classpath}"/>
                        <attribute name="Main-Class" value="${main.class}"/>
                    </manifest>
                </jar>
            </then>
            <else>
                <property name="manifest.classpath" value="${jar.classpath}"/>

                <!--Build the jar file-->
                <jar destfile="${jar.file}">
                    <fileset dir="${classes.dir}"/>
                    <fileset dir="${target.dir}">
                        <include name="assets/**"/>
                    </fileset>
                    <manifest>
                        <attribute name="Built-By" value="${user.name}"/>
                        <attribute name="Class-Path" value="${manifest.classpath}"/>
                        <attribute name="Main-Class" value="${main.class}"/>
                    </manifest>
                </jar>
            </else>
        </if>
    </target>

    <target name="-run-init">

        <!--Create program execution/run directory-->
        <mkdir dir="${run.dir}"/>

        <!-- Control debug mode port so that other users, if not in debug, wouldn't have to worry about debug port collision -->
        <if>
            <equals arg1="${suspend.debug}" arg2="y"/>
            <then>
                <property name="debug.app.args" value="-agentlib:jdwp=transport=dt_socket,server=y,suspend=${suspend.debug},address=${remote.debug.port}"/>
            </then>
            <else>
                <property name="debug.app.args" value="-Ddebug.app.dummy.args=y"/>
            </else>
        </if>

        <!-- Set default JVM args in case not set by the user. -->
        <property name="jvm.args" value="-Ddummy.args"/>
    </target>

    <!-- ===================================
         target: run

         run.main.class sets the class to run
         run.classpath sets the classpath to run the run.main.class with. If not set, ant searches for ALL jars under target.dir
           OR
         run.jar sets the executable jar to run
         =================================== -->
    <target name="run" depends="-run-init" description="Runs the project">

        <echo message="Running with java version: ${java.version}"/>

        <!-- Resolve jar name in case pattern is provided -->
        <path id="run.jar.path.id">
            <fileset file="${run.jar}"></fileset>
        </path>
        <property name="run.jar.path" refid="run.jar.path.id"/>

        <!-- Execute the java command with the supplied run jar -->
        <java dir="${run.dir}" jar="${run.jar.path}" fork="true" spawn="false"
              jvm="${env.JAVA_HOME}/bin/java">
            <jvmarg value="${jvm.args}"/>
            <jvmarg value="${debug.app.args}"/>
            <arg line="${java.run.arguments}"/>
        </java>
    </target>

    <!-- =================================
         target: -init-prime-db

         A pre prime-db init target
         ================================= -->
    <target name="-init-prime-db">

        <!-- Control debug mode port so that other users, if not in debug, wouldn't have to worry about debug port collision -->
        <if>
            <equals arg1="${suspend.debug}" arg2="y"/>
            <then>
                <property name="debug.app.args" value="-agentlib:jdwp=transport=dt_socket,server=y,suspend=${suspend.debug},address=${remote.debug.port}"/>
            </then>
            <else>
                <property name="debug.app.args" value="-Ddebug.app.dummy.args=y"/>
            </else>
        </if>

        <!-- Set default JVM args in case not set by the user. -->
        <property name="db.jvm.args" value="-Ddummy.args"/>

    </target>

    <!-- =================================
         target: prime-db

         The target reads the h2.sql script and primes the H@ DB with it.
         ================================= -->
    <target name="prime-db" depends="-init-prime-db" description="primes the H2 database with some data specified by data/h2.csv">

        <property name="db.java.run.arguments"
                  value="-user ${db.user} -password ${db.password} -driver ${db.driver} -url ${db.url} -script ${db.prime.script}"/>

        <path id="db.run.classpath.id">
            <fileset dir="${lib.dir}">
                <filename name="h2*.jar"/>
            </fileset>
        </path>

        <echo message="Running with java version: ${java.version}"/>
        <java dir="${run.dir}" classpathref="db.run.classpath.id" classname="${db.run.main.class}" fork="true"
              spawn="false" jvm="${env.JAVA_HOME}/bin/java">
            <jvmarg value="${db.jvm.args}"/>
            <jvmarg value="${debug.app.args}"/>
            <arg value="-user"/>
            <arg value="${db.user}"/>
            <arg value="-password"/>
            <arg value="${db.password}"/>
            <arg value="-driver"/>
            <arg value="${db.driver}"/>
            <arg value="-url"/>
            <arg value="${db.url}"/>
            <arg value="-script"/>
            <arg value="${db.prime.script}"/>
        </java>

    </target>

</project>
