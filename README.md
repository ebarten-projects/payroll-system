# Payroll System

## Setup & Pre-requisites
1. ANT v1.10.1 installed
2. ANT_HOME environment variable defined
3. PATH environment variable includes: ANT_HOME/bin
4. Download Ivy v2.4.0 from `http://apache.mirror.amaze.com.au//ant/ivy/2.4.0/apache-ivy-2.4.0-bin.zip` and extract `ivy-2.4.0.jar` into ANT_HOME/lib folder.
5. Download ANT Contrib v1.0b3 from `https://sourceforge.net/projects/ant-contrib/files/ant-contrib/1.0b3/ant-contrib-1.0b3-bin.zip/download` and extract `ant-contrib-1.0b3.jar` into ANT_HOME/lib folder.

## Build
  
1. Run `npm install` in the src/main/webapp folder.
2. Run `ant` at the repository base directory to build the project. The build artifacts will be stored in the `target/` directory.

## Priming the DB (optional)
1. Edit `data/h2.sql` script to contain the data you would like to prime the database with.
2. Make sure your server is not running and there the DB connection is not in user and -
3. Run `ant prime-db`

## Running

Run `ant run` to execute the Payroll System and browse to: `http://localhost`


## Further help

The system is using H2 file based database.
The DB name is: h2.db. In order to browse into the DB you need to stop the server as only single access is allowed with this DB. Once that is done, in the repository base directory, you can run: `java -cp target/lib/h2-1.4.193.jar org.h2.tools.Server`

This will open up a new tab in your default browse with a user interface to allow you to connect to the DB.
Use the following credentials:

Field | Value
:-----|:-----
`Saved Settings` |Generic H2 (Embedded)
`Setting Name`   |Generic H2 (Embedded)
`Driver Class`   |org.h2.Driver
`JDBC URL`       |jdbc:h2:[REPO_HOME]/target/run/h2.db
`User Name`      |root
`Password`       |root